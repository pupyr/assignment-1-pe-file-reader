/// @file 
/// @brief Main application file
#include "file_worker.h"
#include "errMass.h"
#include <inttypes.h>
#include <stdio.h>
#include <string.h>

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code

int main(int argc, char** argv){
  (void) argc; (void) argv;
  if(argc!=4){
    usage(stderr);
    return 1;
  }

  else{

  /// файл, из которого берется секция
  FILE* in_file=fopen(argv[1], "rb");
  /// файл, в который запишется секция
  FILE* out_file=fopen(argv[3], "wb");
  if(in_file==0){
    openErr(stderr);
    fclose(in_file);
    fclose(out_file);
    return 1;
  }

  if(readSection(in_file, out_file, argv[2])){
    sectionWorkErr(stderr);
    fclose(in_file);
    fclose(out_file);
    return 1;
  };


  fclose(in_file);
  fclose(out_file);

  }
  return 0;
}

