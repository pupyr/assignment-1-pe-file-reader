/// @file
/// @brief Метод для работы с секцией
#include "PE.h"
#include "errMass.h"
#include "file_worker.h"
#include <inttypes.h>
#include <stdio.h>
#include <string.h>

/// @brief Метод, который достает нужную секцию и записывает ее в файл
/// @param in_file файл, из которого берется секция
/// @param output_file файл, в который запишется секция
/// @param section название секции
/// @return результат работы (если 1, то была ошибка)

int readSection(FILE* in_file, FILE* output_file, char* section){
  struct PEFile pe;
  if(!fread(&pe, sizeof(pe), 1, in_file)){
        readErr(stderr, "peHeaders");
        return 1;
  };
   int64_t offOpt=(int64_t) (pe.header.SizeofOptionalHeader-sizeof(struct OptionalHeader));
        if(fseek(in_file,offOpt, SEEK_CUR)){
            readErr(stderr, "another options");
            return 1;
    };
  
  struct SectionHeader sh[pe.header.NumOfSect];
  int address=0;
  int size=1;
   for(int i=0; i<pe.header.NumOfSect; i++){
        if(!fread(&sh[i], sizeof(struct SectionHeader), 1, in_file)){
            readErr(stderr, "Sections");
            return 1;
        };
      if(!strcmp(section,(char*) sh[i].name)){
        address=sh[i].RawAddress;
        size=sh[i].RawSize;
      }
   }

    if(fseek(in_file,address, SEEK_SET)){
            readErr(stderr, "other beetwean sections");
            return 1;
    };

    char data[size];

   if(!fread(&data, size, 1, in_file)){
            readErr(stderr, "nedded section");
            return 1;
    };
    if(!fwrite(&data, size, 1, output_file)){
            writeErr(stderr);
            return 1;
    };
   return 0;
}

