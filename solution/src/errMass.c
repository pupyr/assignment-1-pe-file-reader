/// @file
/// @brief Методы вывода об ошибках
#include <stdio.h>

#define APP_NAME "section-extractor"
 /// @brief метод для вывода ошибки о некорректном вводе
void usage(FILE *f)
{
  fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief метод для вывода ошибки открытия файла

void openErr(FILE *f){
    fprintf(f,"Wrong file path\n");
}


 /// @brief метод для вывода ошибки чтения файла
 /// @param str место, где произошла ошибка

void readErr(FILE *f, char* str){
    fprintf(f,"Some read error: %s\n", str);
}


/// @brief метод для вывода ошибки записи файда

void writeErr(FILE *f){
    fprintf(f,"Some write error\n");
}


/// @brief метод для вывода ошибки работы с секцией

void sectionWorkErr(FILE *f){
    fprintf(f,"Some error in sectionWorker\n");
}

