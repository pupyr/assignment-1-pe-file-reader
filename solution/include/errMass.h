#include <stdio.h>

void usage(FILE *f);

void openErr(FILE *f);

void readErr(FILE *f, char* str);

void writeErr(FILE *f);

void sectionWorkErr(FILE *f);
