/**
 \file
*/
/// @brief Объявление PE структуры и его заголовков
#include <stdint.h>

#ifdef _MSC_VER
#pragma packed(push, 1)
#endif

/// @brief Структура PE заголовков
struct
#if defined __clang__ || defined __GNUC__
__attribute__((packed))
#endif
PEHeader{
    int16_t machineVers;

    int16_t NumOfSect;
    
    int32_t TimeDate;

    int32_t Pointer;

    int32_t NumOfSimb;

    int16_t SizeofOptionalHeader;

    int16_t Characteristics;
};

#ifdef _MSC_VER
#pragma packed(pop)
#endif


#ifdef _MSC_VER
#pragma packed(push, 1)
#endif

/// @brief Структура опциональных PE заголовков
struct
#if defined __clang__ || defined __GNUC__
__attribute__((packed))
#endif
OptionalHeader{
    int16_t magic;

    int8_t MajorLink;

    int8_t MinorLink;

    int32_t sizeOfCode;

    int32_t sizeOfInitData;

    int32_t sizeOfUninitData;

    int32_t addresOfEntryPoint;

    int32_t BaseOfCode;

    int32_t BaseofData;

    int32_t ImageBase;

    int32_t SectionAlig;

    int32_t FileAlig;

    int16_t MajorOperation;

    int16_t MinorOperation;

    int16_t MajorImage;

    int16_t MinorImage;

    int16_t MajorSubsys;

    int16_t MinorSubsys;

    int32_t Win32;

    int32_t SizeOfImage;

    int32_t SizeOfHeaders;

    int32_t CheckSum;

    int16_t Subsystem;

    int16_t Dll;

    int32_t SizeOfStackReserved;

    int32_t SizeOfStackCommit;

    int32_t SizeOfHeapReserved;

    int32_t SizeOfHeapCommit;

    int32_t Loader;

    int32_t NumOfRvaAndSizes;

};

#ifdef _MSC_VER
#pragma packed(pop)
#endif


#ifdef _MSC_VER
#pragma packed(push, 1)
#endif

/// @brief Структура заголовков секций
struct
#if defined __clang__ || defined __GNUC__
__attribute__((packed))
#endif
 SectionHeader{

    int8_t name[8];
    
    int32_t VirtSize;
    
    int32_t VirtAdrress;

    int32_t RawSize;

    int32_t RawAddress;

    int32_t RellocAddress;

    int32_t LineNum;

    int16_t RelocNum;

    int16_t LineNums;

    int32_t Characteristics;

};

#ifdef _MSC_VER
#pragma packed(pop)
#endif

#ifdef _MSC_VER
#pragma packed(push, 1)
#endif

/// @brief Структура PE файла
struct
#if defined __clang__ || defined __GNUC__
__attribute__((packed))
#endif
 PEFile
{
  char dos[252];
  /// Main header
  struct PEHeader header;
  /// Optional header
  struct OptionalHeader optional_header;
};


#ifdef _MSC_VER
#pragma packed(pop)
#endif

